import cv2
import imutils
import numpy as np
import math
import time
import Image
import pytesseract
import pickle
import string
from SNN import SNN	


class SNN:
	
	def __init__(self,snnLearning_rate,snnNetwork_structure,snnError,snnWeight,snnBias,snnDelta,Map,startIndex,endIndex):
		self.Map = Map	
		self.startIndex	= startIndex
		self.endIndex = endIndex
		self.learning_rate = snnLearning_rate		
		self.network_structure = snnNetwork_structure
		self.error = snnError
		self.weight = snnWeight
		self.bias = snnBias
		self.delta = snnDelta
		self.characterMap = np.concatenate((np.asarray(range(0,10)),np.asarray(list(string.uppercase)),np.asarray(list(string.lowercase))))

	def softmax(self,x):
		e = np.exp(x)
		row_add = np.sum(e,axis=1)
		e = np.transpose(np.transpose(e)/row_add) 	
		return e
		
	def g(self,x):
		#return np.tanh(x)
		return 1.0/(1.0 + np.exp(-x))
	
			
	def forward_prop(self,inp):
		self.a = []
		self.z = []
		self.z.append([None])
		self.a.append(inp)
		temp_i = len(self.network_structure)-1				
		for i in range(1,temp_i):
			self.z.append(np.dot(self.a[i-1],self.weight[i]) + self.bias[i])
			self.a.append(self.g(self.z[-1]))
		self.z.append(np.dot(self.a[temp_i-1],self.weight[temp_i]) + self.bias[temp_i])
		self.a.append(self.softmax(self.z[-1]))	
	
	def predict(self,inp):
		self.forward_prop(inp)
		index = np.argmax(self.a[-1],axis = 1)
		return index

def imageDisplay(title,imageName):
	cv2.imshow(title,imageName)
	cv2.waitKey(0)

def resizeImageUnit(image,resizedHeight):
	return imutils.resize(image, height = resizedHeight)	

def getAspectRatio(image):
	return float(image.shape[0])/(image.shape[1])
	
def getOriginalImageI0(imagePath):
	image = cv2.imread(imagePath)
	#image = cv2.transpose(image)
	#image = cv2.flip(image,1)
	return image

def getGrayscaleImage(image):
	return cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)	

def getThresholdImage(image,threshold):
	retval,thresholdImage = cv2.threshold(image,threshold,255,cv2.THRESH_BINARY)
	return thresholdImage

def getCropImage(image,guidingImage):
	croppingRectangle = getCroppingRectangle(guidingImage)
	croppingRectangleX = croppingRectangle[0]
	croppingRectangleY = croppingRectangle[1]
	croppingRectangleW = croppingRectangle[2]
	croppingRectangleH = croppingRectangle[3]
		
	croppedImage = image[croppingRectangleY:croppingRectangleY+croppingRectangleH,
				croppingRectangleX:croppingRectangleX+croppingRectangleW]
	croppedImage = getThresholdImage(croppedImage,110)

	imageDisplay("croppedImage",croppedImage)
	return croppedImage

def getCroppingRectangle(guidingImage):
	(imageContours, _) = cv2.findContours(guidingImage.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
	imageContours = sorted(imageContours, key = cv2.contourArea, reverse = True)

	croppingContour = imageContours[0]
	croppingRectangle = cv2.boundingRect(croppingContour)
	
	return croppingRectangle

def getNegative(imageWhitePaper):
	return (255-imageWhitePaper)
	

def bleed(image,vector,iterate):
	kernelHorizotal = np.array(vector,np.uint8)
	bleedingImage = cv2.dilate(image,kernelHorizotal,iterations = iterate)

	imageDisplay("bleedingImage",bleedingImage)
	
	return bleedingImage



def textContoursSort(a):
	xa,ya,wa,ha = cv2.boundingRect(a)
	return (-ya + (float(xa)/10000))

def characterContoursSort(a):
	xa,ya,wa,ha = cv2.boundingRect(a)
	return -xa


def getTextImages(imageWhitePaper):
	imageBlackPaper = getNegative(imageWhitePaper)
	imageDisplay("imageBlackPaper",imageBlackPaper)
	
	bleedingTextImageBlack = bleed(imageBlackPaper,[[1,1,1,0,0]],10)
	bleedingTextImageWhite = getNegative(bleedingTextImageBlack)
	
	

	(textContours, _) = cv2.findContours(bleedingTextImageWhite.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
	textContours = sorted(textContours, key = textContoursSort, reverse = True)
	textImagesList = []	
	for i in range(1,len(textContours)):	
		tc = textContours[i]
		tcX,tcY,tcW,tcH = cv2.boundingRect(tc)
		textImage = imageWhitePaper[tcY:tcY+tcH,tcX:tcX+tcW]
		tcImageInfo = {'tcx':tcX, 'tcy':tcY, 'tcw':tcW, 'tch':tcH,'textImage':textImage}
		textImagesList.append(tcImageInfo)

	return textImagesList

def getCharacterImages(textImage,snnList):
	textImageWhite = textImage['textImage']
	textImageBlack = getNegative(textImageWhite)
	bleedingCharacterImageBlack = bleed(textImageBlack,[[0,1,0],[0,1,0],[0,1,0]],10)
	bleedingCharacterImageWhite = getNegative(bleedingCharacterImageBlack)
	imageDisplay("textImageWhite",textImageWhite)
	
	(characterContours, _) = cv2.findContours(bleedingCharacterImageBlack.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
	characterContours = sorted(characterContours, key = characterContoursSort, reverse = True)
	characterImagesList = []	
	for i in range(0,len(characterContours)):	
		cc = characterContours[i]
		ccX,ccY,ccW,ccH = cv2.boundingRect(cc)
		characterImage = textImageWhite[ccY:ccY+ccH,ccX:ccX+ccW]
		#characterImage = cv2.copyMakeBorder(characterImage,10,10,10,10,cv2.BORDER_CONSTANT,value=(255,255,255))
		characterImage =  cv2.resize(characterImage, (28,28), interpolation = cv2.INTER_AREA) 
		characterImageFlat = characterImage.flatten()
		characterImageFlat = np.array([characterImageFlat])
		characterImageFlat = characterImageFlat/255.0
 

		imageDisplay("characterImage",characterImage)
		for snn in snnList:			
			prediction = snn.Map[snn.predict(characterImageFlat)]
			print prediction,
		print ""

		im = Image.fromarray(characterImage)
		#imageDisplay("characterImage",characterImage)
		#print pytesseract.image_to_string(im)
		
		
		ccImageInfo = {'ccx':ccX, 'ccy':ccY, 'ccw':ccW, 'cch':ccH,'characterImage':characterImage}
		characterImagesList.append(ccImageInfo)
					
		#print characterImage
		#print "----------------------------------------"
	return characterImagesList


def main(imagePath):
	
	# Getting back the objects:
	snnDigit = []
	snnLowerLetter = []
	snnUpperLetter = []
	lowerLetterMap = np.asarray(list(string.lowercase))
	upperLetterMap = np.asarray(list(string.uppercase))
	digitMap = np.asarray(range(0,10))
	with open('pickle_files/digit.pickle','rb') as f:
		snnLearning_rate,snnNetwork_structure,snnError,snnWeight,snnBias,snnDelta = pickle.load(f)
		
		snnDigit = SNN(snnLearning_rate,snnNetwork_structure,snnError,snnWeight,snnBias,snnDelta,digitMap,1,10)
	
	with open('pickle_files/lowerLetter.pickle','rb') as f:
		snnLearning_rate,snnNetwork_structure,snnError,snnWeight,snnBias,snnDelta = pickle.load(f)
		snnLowerLetter = SNN(snnLearning_rate,snnNetwork_structure,snnError,snnWeight,snnBias,snnDelta,lowerLetterMap,11,36)
	
	with open('pickle_files/upperLetter.pickle','rb') as f:
		snnLearning_rate,snnNetwork_structure,snnError,snnWeight,snnBias,snnDelta = pickle.load(f)
		snnUpperLetter = SNN(snnLearning_rate,snnNetwork_structure,snnError,snnWeight,snnBias,snnDelta,upperLetterMap,37,62)
		
	startTime = time.time()
	originalImageI0 = getOriginalImageI0(imagePath)
	
	resizedHeight = 1500
	resizedImageI1 = resizeImageUnit(originalImageI0,resizedHeight)
	imageDisplay("resizedImageI1",resizedImageI1)
	aspectRatioResizedImageI1 = getAspectRatio(resizedImageI1)

	grayscaleImageI2 = getGrayscaleImage(resizedImageI1)
	imageDisplay("grayscaleImageI2",grayscaleImageI2)
	
	thresholdImageI3 = getThresholdImage(grayscaleImageI2,90)
	imageDisplay("thresholdImageI3",thresholdImageI3)

	croppedImageI4 = getCropImage(grayscaleImageI2,thresholdImageI3)
	
	textImagesList = getTextImages(croppedImageI4)
	completeCharactersList = []
	for TI in textImagesList:
		textImage = TI['textImage']
		characterImagesList = getCharacterImages(TI,[snnDigit, snnLowerLetter, snnUpperLetter])
		completeCharactersList.append([TI,characterImagesList])
		
	#print completeCharactersList
	print "RunningTime",time.time()-startTime

	
if __name__ == "__main__":	
	#main("data/textArea01.png")
	main("data/receipt1.JPG")


