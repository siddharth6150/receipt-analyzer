import math
import numpy as np
from os import listdir
from os.path import isfile, join
import cv2
import time
import string
import pickle

def getThresholdImage(image,threshold):
	retval,thresholdImage = cv2.threshold(image,threshold,255,cv2.THRESH_BINARY)
	return thresholdImage

def getCropImage(image,guidingImage):
	croppingRectangle = getCroppingRectangle(guidingImage)
	croppingRectangleX = croppingRectangle[0]
	croppingRectangleY = croppingRectangle[1]
	croppingRectangleW = croppingRectangle[2]
	croppingRectangleH = croppingRectangle[3]
		
	croppedImage = image[croppingRectangleY:croppingRectangleY+croppingRectangleH,
				croppingRectangleX:croppingRectangleX+croppingRectangleW]
	croppedImage = getThresholdImage(croppedImage,110)

	#imageDisplay("croppedImage",croppedImage)
	return croppedImage

def getCroppingRectangle(guidingImage):
	(imageContours, _) = cv2.findContours(guidingImage.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
	imageContours = sorted(imageContours, key = cv2.contourArea, reverse = True)

	croppingContour = imageContours[0]
	croppingRectangle = cv2.boundingRect(croppingContour)
	
	return croppingRectangle


def imageDisplay(title,imageName):
	cv2.imshow(title,imageName)
	cv2.waitKey(0)

def getNegative(image):
	return (255-image)
	

def g(x):
	#return np.tanh(x)
	return 1.0/(1.0 + np.exp(-x))
		
def g_grad(x):
	#return 1 - pow(np.tanh(x),2)
	return g(x)*(1-g(x))

def softmax(x):
	e = np.exp(x)
	row_add = np.sum(e,axis=1)
	e = np.transpose(np.transpose(e)/row_add) 	
	return e
class SNN:
	learning_rate = 0.002
	network_structure = []
	error = []
	weight = []
	bias = []
	delta = []
	a = []
	z = []
	def __init__(self,network_structure,startIndex,endIndex,Map):
		self.a = []
		self.z = []
		self.network_structure = network_structure
		self.startIndex = startIndex
		self.endIndex = endIndex
		self.error.append([None])
		self.weight.append([None])
		self.bias.append([None])
		self.delta.append([None])
		self.Map = Map
		for i in range(1,len(self.network_structure)):
			self.error.append(np.zeros(self.network_structure[i],dtype = np.float))
			self.weight.append(np.random.rand(self.network_structure[i-1],self.network_structure[i])-np.multiply(np.ones((self.network_structure[i-1],self.network_structure[i])),0.5))	
			
			self.bias.append(np.random.rand(1,self.network_structure[i])-np.multiply(np.ones(1,self.network_structure[i]),0.5))
			self.delta.append(np.random.rand(1,self.network_structure[i])-np.multiply(np.ones(1,self.network_structure[i]),0.5))

			
				
	def forward_prop(self,inp):
		self.a = []
		self.z = []
		self.z.append([None])
		self.a.append(inp)
		temp_i = len(self.network_structure)-1				
		for i in range(1,temp_i):
			self.z.append(np.dot(self.a[i-1],self.weight[i]) + self.bias[i])
			self.a.append(g(self.z[-1]))
		self.z.append(np.dot(self.a[temp_i-1],self.weight[temp_i]) + self.bias[temp_i])
		self.a.append(softmax(self.z[-1]))
	

	def backward_prop(self,out):
		self.delta[-1] = self.a[-1] - out
		for i in range(len(self.network_structure)-2,0,-1):		
			self.delta[i] = np.multiply(g_grad(self.z[i]),np.dot(self.delta[i+1],np.transpose(self.weight[i+1])))	
			
		for i in range(1,len(self.network_structure)):			
			self.error[i] = np.dot(np.transpose(self.a[i-1]),self.delta[i])
			self.weight[i] -= np.multiply(self.error[i],self.learning_rate)
			self.bias[i] -= np.multiply(self.delta[i],self.learning_rate)
			

	def train(self,iterations=1):
		for it in range(0,iterations):
			print "iteration:",it
			
			for j in range(1,500):
				sampleNumber = '{0:05}'.format(j)							
				for i in range(self.startIndex,self.endIndex+1):
					sampleType = '{0:03}'.format(i)
					filePath = "English/Fnt/Sample" + sampleType + "/" + "img" + sampleType + "-" + sampleNumber + ".png"
					print filePath
					inp = cv2.imread(filePath)
					inp = cv2.cvtColor(inp,cv2.COLOR_BGR2GRAY)
					inpBlack = getNegative(inp)
					inpBlackThreshold = getThresholdImage(inpBlack,50)
					inpBlackCropped = getCropImage(inpBlack,inpBlackThreshold)
					inpWhiteCropped = getNegative(inpBlackCropped)
					inp = cv2.resize(inpWhiteCropped, (28,28), interpolation = cv2.INTER_AREA)
					#imageDisplay("inp",inp)
					inp = inp.flatten()
					inp = np.array([inp])
					inp = inp/255.0
	 				
					out = np.zeros((1,self.endIndex-self.startIndex+1))
					out[0][i-self.startIndex] = 1
				
					self.forward_prop(inp)
					self.backward_prop(out)
		
		
	def printParams(self):
		print "network_structure",self.network_structure
		print "error",self.error
		print "weight",self.weight
		print "bias",self.bias				

	def predict(self,inp):
		self.forward_prop(inp)
		index = np.argmax(self.a[-1],axis = 1) + self.startIndex
		return index

	def checking(self):
		correct_count = 0
		total_count = 0
		for j in range(501,1000):
			sampleNumber = '{0:05}'.format(j)							
			for i in range(self.startIndex,self.endIndex+1):
				sampleType = '{0:03}'.format(i)
				filePath = "English/Fnt/Sample" + sampleType + "/" + "img" + sampleType + "-" + sampleNumber + ".png"
				inp = cv2.imread(filePath)
				inp = cv2.cvtColor(inp,cv2.COLOR_BGR2GRAY)
				inpBlack = getNegative(inp)
				inpBlackThreshold = getThresholdImage(inpBlack,50)
				inpBlackCropped = getCropImage(inpBlack,inpBlackThreshold)
				inpWhiteCropped = getNegative(inpBlackCropped)
				inp = cv2.resize(inpWhiteCropped, (28,28), interpolation = cv2.INTER_AREA)
				#imageDisplay("image",inp)
				inp = inp.flatten()
				inp = np.array([inp])
				inp = inp/255.0
				#print im

			 		
			 	prediction = self.predict(inp)
				
				#if(prediction == i or (i > 10 and (prediction - i)%26 == 0)):
				if(prediction == i):
					correct_count += 1
				print self.Map[i-self.startIndex],"-->>",self.Map[prediction-self.startIndex]
				total_count += 1

		print correct_count,total_count,float(correct_count)/total_count


if __name__ == "__main__":
	
	
	network_structure_digit = np.array([784,400,10])
	network_structure_lower_letter = np.array([784,400,26])
	network_structure_upper_letter = np.array([784,400,26])
	
	lowerLetterMap = np.asarray(list(string.lowercase))
	upperLetterMap = np.asarray(list(string.uppercase))
	digitMap = np.asarray(range(0,10))

	if(False):
		snnDigit = SNN(network_structure_digit,1,10,digitMap)
		snnDigit.train()
		#snnDigit.checking()
		with open('pickle_files/digit.pickle', 'w') as f:
		    pickle.dump([snnDigit.learning_rate,
				snnDigit.network_structure,
				snnDigit.error,
				snnDigit.weight,
				snnDigit.bias,
				snnDigit.delta], f)

		
	if(False):
	
		snnLowerLetter = SNN(network_structure_lower_letter,11,36,lowerLetterMap)
		snnLowerLetter.train()
		#snnLowerLetter.checking()
		with open('pickle_files/lowerLetter.pickle', 'w') as f:
		    pickle.dump([snnLowerLetter.learning_rate,
				snnLowerLetter.network_structure,
				snnLowerLetter.error,
				snnLowerLetter.weight,
				snnLowerLetter.bias,
				snnLowerLetter.delta], f)		
	if(True):
		snnUpperLetter = SNN(network_structure_upper_letter,37,62,upperLetterMap)
		snnUpperLetter.train()
		snnUpperLetter.checking()
		with open('pickle_files/upperLetter.pickle', 'w') as f:
		    pickle.dump([snnUpperLetter.learning_rate,
				snnUpperLetter.network_structure,
				snnUpperLetter.error,
				snnUpperLetter.weight,
				snnUpperLetter.bias,
				snnUpperLetter.delta], f)	
			
